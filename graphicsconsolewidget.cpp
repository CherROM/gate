#include "graphicsconsolewidget.h"

GraphicsConsoleWidget::GraphicsConsoleWidget(QWidget *parent, Qt::WindowFlags f) : QLabel(parent)
{
    setWindowFlags(f);
}

GraphicsConsoleWidget::~GraphicsConsoleWidget(){}

void GraphicsConsoleWidget::setStream(QTextStream *stream)
{
    kbStream = stream;
}

void GraphicsConsoleWidget::update(const Screen &s)
{
    setPixmap(QPixmap::fromImage(s.getImage()));
}

void GraphicsConsoleWidget::keyPressEvent(QKeyEvent *e)
{
    QString temp = e->text();

    if(!temp.isEmpty())
    {
        QChar key = temp.at(0);

        *kbStream << key;
    }
}

