#ifndef SCREENWIDGET_H
#define SCREENWIDGET_H

#include <QWidget>
#include <graphicsconsolewidget.h>
#include <textconsolewidget.h>
#include <QTextStream>
#include "screen.h"

namespace Ui {
class ScreenWidget;
}

class ScreenWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ScreenWidget(QWidget *parent = 0, Qt::WindowFlags f = 0, QTextStream *kbStream = nullptr);
    ~ScreenWidget();

    void update(const Screen &s);
    void setStream(QTextStream *kbStream);

private:
    Ui::ScreenWidget *ui;
};

#endif // SCREENWIDGET_H
