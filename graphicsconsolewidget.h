#ifndef GRAPHICSCONSOLEWIDGET_H
#define GRAPHICSCONSOLEWIDGET_H

#include <QLabel>
#include <QTextStream>
#include <QKeyEvent>
#include <iostream>
#include "screen.h"

class GraphicsConsoleWidget : public QLabel
{
    Q_OBJECT
private:
    QTextStream* kbStream;
public:
    explicit GraphicsConsoleWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);
    ~GraphicsConsoleWidget();

    void setStream(QTextStream *stream);

    void update(const Screen& s);

    void keyPressEvent(QKeyEvent *e);
};

#endif // GRAPHICSCONSOLEWIDGET_H
