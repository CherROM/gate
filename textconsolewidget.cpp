#include "textconsolewidget.h"

TextConsoleWidget::TextConsoleWidget(QWidget *parent, Qt::WindowFlags f) : QPlainTextEdit(parent)
{
    setWindowFlags(f);

#ifdef Q_OS_WIN32
    setFont(QFont("Consolas", 10));
#endif
#ifdef Q_OS_LINUX
    setFont(QFont("Monospace", 9));
#endif

    setFixedSize(571, 386);
    setStyleSheet("color: white; background-color: black;");
    setReadOnly(true);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void TextConsoleWidget::setStream(QTextStream* stream)
{
    kbStream = stream;
}

void TextConsoleWidget::wheelEvent(QWheelEvent *e)
{
    e->ignore();
}

void TextConsoleWidget::keyPressEvent(QKeyEvent *e)
{
    QString temp = e->text();

    if(!temp.isEmpty())
    {
        QChar key = temp.at(0);

        *kbStream << key;
    }
}

void TextConsoleWidget::update(const Screen &s)
{
    clear();

    QTextCursor cur = textCursor();
    setTextCursor(cur);

    const std::vector<std::vector<QChar>> field =  s.getCharField();

    QColor fg = Qt::white;
    QColor bg = Qt::black;

    QTextCharFormat format;

    format.setBackground(bg);
    format.setForeground(fg);

    cur.mergeCharFormat(format);

    cur.movePosition(QTextCursor::Start);

    QString line;

    for(int i = 0; i < s.getScreenHeight(); ++i)
    {
        for(int j = 0; j < s.getScreenWidth(); ++j)
        {
            line.append(field[i][j]);
            //            ScreenCell::Color bgColor = field[i][j].bgColor;
            //            bg.setRgb(bgColor.red, bgColor.green, bgColor.blue);

            //            ScreenCell::Color fgColor = field[i][j].fgColor;
            //            bg.setRgb(fgColor.red, fgColor.green, fgColor.blue);

            //            format.setBackground(bg);
            //            format.setForeground(fg);

            //            cur.mergeCharFormat(format);

            //            cur.movePosition(QTextCursor::NextCharacter);
        }
        if(i != (s.getScreenHeight() - 1)) line.append('\n');

        cur.insertText(line);
        line.clear();
    }
}
