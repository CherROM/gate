//
// Created by chromean on 21/10/2015.
//

#include "register.h"

void Register::set(const unsigned short &s)
{
    val = s;
    high = ((val & 0xff00) >> 8);
    low = (val & 0x00ff);
}

void Register::setLow(const unsigned char &b)
{
    low = b;
    val = ((high<<8) + low);
}

void Register::setHigh(const unsigned char &b)
{
    high = b;
    val = ((high<<8) + low);
}

Register::Register() : val(0), high(0), low(0) {}

Register::Register(unsigned short v) : val(v),
    high((val & 0xff00) >> 8),
    low(val & 0x00ff) {}

unsigned short Register::get() const
{
    return val;
}

unsigned char Register::getLow() const
{
    return low;
}

unsigned char Register::getHigh() const
{
    return high;
}
